/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
'use strict';
module RVS{
	export class GridCollection extends Backbone.Collection{
		model=function(attr,options){
			return new RVS.GridModel(attr,options);
		}
		constructor(options={}){
			super(options);
			_.bindAll(this, 'flipInbetweenStones','findAchievableGrid','testAchievable','getConquerableGrids');
		}
		flipInbetweenStones(model,val):RVS.GridModel[]{
			var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
			var turn = gameModel.get('turn');
			var myColor = turn + 1;
			// 着手した点から8方向を捜査して挟まってたらひっくり返す
			var modelArray = this.getConquerableGrids(model,myColor,true);
			_.each(modelArray,(model)=>{
				model.set('status',myColor);
			});
			// ToDo: 本当はターンを司るコントローラを外に作るべき
			// 相手のターン
			turn = turn*-1+1;
			gameModel.set('turn',turn);
			return modelArray;
		}
		/***
		有効なマスの発見
		*/
		findAchievableGrid():RVS.GridModel{
			var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
			var turn = gameModel.get('turn');
			var myColor = turn + 1;
			this.each((model,i)=>{
				model.set({'isAchievable':this.testAchievable(model,myColor)})
			});
			var model = <RVS.GridModel> this.findWhere({'isAchievable':true});
			if(!model){
				alert('パス！');
				// ToDo: 本当はターンを司るコントローラを外に作るべき
				// 相手のターン
				turn = turn*-1+1;
				gameModel.set('turn',turn);
				myColor = turn + 1;
				// もっかい探索
				this.each((model,i)=>{
					model.set({'isAchievable':this.testAchievable(model,myColor)})
				});
				model = <RVS.GridModel> this.findWhere({'isAchievable':true});
				if(!model){
					alert('終了！');
				} else if(turn==0){
					var modelArray = this.where({'isAchievable':true});
					_.each(modelArray,(model)=>{
						model.set({'isAchievable':false});
					})
					var r = Math.floor( Math.random() * (modelArray.length - 1) );
					setTimeout(()=>{
						this.flipInbetweenStones(modelArray[r],myColor);
						modelArray[r].set({'status':myColor,'isAchievable':false});
						setTimeout(()=>{
		           			this.findAchievableGrid();
		           		},200);
	               	},1000);
				}
			} else if(turn==0){
				var modelArray = this.where({'isAchievable':true});
				_.each(modelArray,(model)=>{
					model.set({'isAchievable':false});
				})
				var r = Math.floor( Math.random() * (modelArray.length - 1) );
				setTimeout(()=>{
					this.flipInbetweenStones(modelArray[r],myColor);
					modelArray[r].set({'status':myColor,'isAchievable':false});
					setTimeout(()=>{
               			this.findAchievableGrid();
               		},200);
               	},1000);
			}
			return model;
		}
		testAchievable(model,myColor):boolean{
			var modelArray = this.getConquerableGrids(model,myColor);
			return (modelArray.length !== 0);
		}
		getConquerableGrids(model:any,myColor:number,allFlg:boolean=false):RVS.GridModel[]{
			var oxy = model.id.split('-');
			var ox = Number(oxy[0]);
			var oy = Number(oxy[1]);
			var x,y;
			var id;
			var i,imax=1,j,jmax=1;
			var enemyColor = myColor*-1+3
			var status;
			var stoneArray:number[];
			var modelArray:Backbone.Model[];
			var testModel;
			var returnArray:RVS.GridModel[] = [];
			// マスが空じゃなかったら探索しない
			if(model.attributes.status!==0){
				return returnArray;
			}
			loop1:
			for(i=-1;i<=imax;i++){
				for(j=-1;j<=jmax;j++){
					// 両方0(自分自身)ならスキップ
					if(i===j && i===0){
						continue;
					}
					stoneArray = [];
					modelArray = [];
					x = ox;
					y = oy;
					do{
						x += i;
						y += j;
						id = x+'-'+y;
						testModel = this.get(id);
						if(testModel){
							status = testModel.attributes.status;
							stoneArray.push(status);
							modelArray.push(testModel);
							// 敵色の場合のみ続ける。空白と自分の色が入ったら終わる（＝自分の色は最後にしか入らない）
							if(status !== enemyColor){
								break;
							}
						}else {
							break;
						}
					}while((x > 0 && x <=8) && (y > 0 &&  y <= 8))
					// 二マス以上且つ自分の色が含まれていたらvalid
					if(stoneArray.length>1 && stoneArray.indexOf(myColor) != -1){
						Array.prototype.push.apply(returnArray, modelArray);
						if(!allFlg){
							break loop1;
						}
					}
				}
			}
			return returnArray;
		}
	}
}