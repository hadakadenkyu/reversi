/// <reference path='../main.ts' />
'use strict';
module RVS{
	export class GridModel extends Backbone.Model {
		constructor(attr={},options={}){
			super(attr,options);
		}
        defaults():any{
            return {
            	status:0,
            	isAchievable:false
            }
        }
	}
}