/// <reference path='../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/jquerymobile/jquerymobile.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/underscore/underscore.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/backbone/backbone.d.ts' />
/// <reference path='view/AppView.ts' />
declare var FastClick;
interface JQueryStatic{
	cookie:any;
}
interface JQuery {
  swipe:any;
}
'use strict';
module RVS{
	export function capitaliseFirstLetter(string){
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}
	export function toCamelCase(string){
	    return string.replace(/(\-([a-z]))/g, function($1,$2,$3){return $3.toUpperCase();});
	}
    export var isTouchSupported:boolean = Boolean('ontouchstart' in window && 'orientation' in window);
	export var settings = {};
	export var appView;
	export function initialize(){
		RVS.appView = new RVS.AppView({el:document.body});
	}
	export function mobileInit(){
		$.mobile.defaultPageTransition  = 'none';
	}
	
}