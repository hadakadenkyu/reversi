/// <reference path='../main.ts' />
'use strict';
module RVS{
    export class BaseView extends Backbone.View {
        events:any;
        emptyElement(el?:HTMLElement){
            el = (arguments.length==0)?this.el:el;
            if(el){
                var i=el.childNodes.length-1;
                // 逆順にしないとchildNodesの数が変わる
                for(i;i>=0;i--){
                    el.removeChild(el.childNodes[i]);
                }
            }
        }
    }
}