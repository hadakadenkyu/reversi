/// <reference path='../main.ts' />
'use strict';
module RVS{
    export class BasePageView extends RVS.BaseView {
        app:any;
        constructor(options){
            super(options);
            var self = this;
            this.app = options.app;
            _.bindAll(this, '_onShow','_onPreShow','showPage');
            this.events = (typeof this.events == 'object')?this.events:{};
            this.events=$.extend({
                'pageshow':'_onShow',
                'pagebeforeshow':'_onPreShow'
            },this.events);
            this.delegateEvents();
        }
        showPage(effect:string = 'none'){
            this.$el.change();
        }
        _onShow(){
        }
        _onPreShow(){
        }
    }
}