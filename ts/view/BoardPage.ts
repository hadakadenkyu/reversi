/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
/// <reference path='../collection/GridCollection.ts' />
/// <reference path='./GridView.ts' />

'use strict';
module RVS{
    export class BoardPageView extends BasePageView {
        gameModel:Backbone.Model;
        constructor(options){
            super(options);
            var tds:NodeList = this.el.querySelectorAll('td');
            var tdArray:HTMLElement[] = Array.apply(null,tds);
            var i,imax=8,j,jmax=8;
            var gridId = '';
            var initialData = [];
            var gridData = {};
            for(i=1;i<=imax;i++){
                for(j=1;j<=jmax;j++){
                    gridId = i+'-'+j;
                    gridData = {}
                    gridData['id'] = gridId;
                    initialData.push(gridData);
                }            
            }
            this.gameModel = new Backbone.Model();
            this.listenTo(this.gameModel,'change:turn',(model,val)=>{
                this.$el.attr('data-turn',val);
            });
            this.gameModel.set({turn:1});
            var gridCollection = new RVS.GridCollection;
            gridCollection.set(initialData);
            gridCollection.each(function(model){
                var options = {};
                options['model'] = model;
                var view = new RVS.GridView(options);
                var xy = model.attributes.id.split('-');
                var x = Number(xy[0]);
                var y = Number(xy[1]);
                var i = (x-1)+(y-1)*8;
                var td:HTMLElement = tdArray[i];
                td.appendChild(view.el);
            });
            gridCollection.get('4-4').set('status',1);
            gridCollection.get('4-5').set('status',2);
            gridCollection.get('5-4').set('status',2);
            gridCollection.get('5-5').set('status',1);
            // 着手可能枠探索
            setTimeout(function(){
                gridCollection.findAchievableGrid();
            },100);
        }
        _onShow(){
        }
        _onPreShow(){
            super._onPreShow();
        }
    }
}