/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
'use strict';
module RVS{
	export class GridView extends RVS.BaseView {
		constructor(options={}){
			super(options);
            _.bindAll(this, 'move','putStone','toggleHighlight');
            this.el.className = 'grid';
            this.el.setAttribute('data-status','');
            this.listenTo(this.model,'change:status',this.putStone).listenTo(this.model,'change:isAchievable',this.toggleHighlight);
            this.events = (typeof this.events == 'object')?this.events:{};
            this.events=$.extend({
                'click':'move'
            },this.events);
            this.delegateEvents();
		}
        move(){
            var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
            var turn = gameModel.get('turn');
            var achievable = this.model.get('isAchievable');
            var val;
            if(achievable){
                val = turn+1;
                var gridCollection = <RVS.GridCollection> this.model.collection;
                gridCollection.flipInbetweenStones(this.model,val);
                this.model.set({'status':val,'isAchievable':false});
                gridCollection.findAchievableGrid();
            }
        }
        putStone(model,val){
            var status = '';
            if(val === 1){
                status = 'white';
            } else if(val === 2){
                status = 'black';
            }
            this.el.setAttribute('data-status',status);
        }
        toggleHighlight(model,val){
            this.$el.toggleClass('achievable',val);
        }
	}
}