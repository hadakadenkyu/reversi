/// <reference path='../main.ts' />
/// <reference path='BaseView.ts' />
/// <reference path='BasePageView.ts' />
/// <reference path='BoardPage.ts' />

'use strict';
module RVS{
	export class AppView extends Backbone.View{
		viewModels:{[id:string]:Backbone.View;} = {};
		constructor(options){
			super(options);
            _.bindAll(this, 'setup');
			setTimeout(()=>{
				this.setup(options);
			},0);
		}
		private setup(options){
			var self = this;
			/* 各ページで共通のView,modelを作る */
			/* pageがviewに対応するのでViewをひたすら生成 */
			this.$el.children('[data-role="page"]').each((o,el)=>{
				var id = el.getAttribute('id');
				var options:any = {};
				options.el = el;
				options.app = this;
				var varName = RVS.toCamelCase(id+'-view');
				var modelName = RVS.capitaliseFirstLetter(varName);
				if(RVS.hasOwnProperty(modelName)){
					self.viewModels[varName] = new RVS[modelName](options);
				} else {
					self.viewModels[varName] = new RVS.BasePageView(options);
				}
			});
		}
	}
}