/// <reference path='../main.ts' />
'use strict';
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RVS;
(function (RVS) {
    var BaseView = (function (_super) {
        __extends(BaseView, _super);
        function BaseView() {
            _super.apply(this, arguments);
        }
        BaseView.prototype.emptyElement = function (el) {
            el = (arguments.length == 0) ? this.el : el;
            if (el) {
                var i = el.childNodes.length - 1;

                for (i; i >= 0; i--) {
                    el.removeChild(el.childNodes[i]);
                }
            }
        };
        return BaseView;
    })(Backbone.View);
    RVS.BaseView = BaseView;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
'use strict';
var RVS;
(function (RVS) {
    var BasePageView = (function (_super) {
        __extends(BasePageView, _super);
        function BasePageView(options) {
            _super.call(this, options);
            var self = this;
            this.app = options.app;
            _.bindAll(this, '_onShow', '_onPreShow', 'showPage');
            this.events = (typeof this.events == 'object') ? this.events : {};
            this.events = $.extend({
                'pageshow': '_onShow',
                'pagebeforeshow': '_onPreShow'
            }, this.events);
            this.delegateEvents();
        }
        BasePageView.prototype.showPage = function (effect) {
            if (typeof effect === "undefined") { effect = 'none'; }
            this.$el.change();
        };
        BasePageView.prototype._onShow = function () {
        };
        BasePageView.prototype._onPreShow = function () {
        };
        return BasePageView;
    })(RVS.BaseView);
    RVS.BasePageView = BasePageView;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
'use strict';
var RVS;
(function (RVS) {
    var GridModel = (function (_super) {
        __extends(GridModel, _super);
        function GridModel(attr, options) {
            if (typeof attr === "undefined") { attr = {}; }
            if (typeof options === "undefined") { options = {}; }
            _super.call(this, attr, options);
        }
        GridModel.prototype.defaults = function () {
            return {
                status: 0,
                isAchievable: false
            };
        };
        return GridModel;
    })(Backbone.Model);
    RVS.GridModel = GridModel;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
'use strict';
var RVS;
(function (RVS) {
    var GridCollection = (function (_super) {
        __extends(GridCollection, _super);
        function GridCollection(options) {
            if (typeof options === "undefined") { options = {}; }
            _super.call(this, options);
            this.model = function (attr, options) {
                return new RVS.GridModel(attr, options);
            };
            _.bindAll(this, 'flipInbetweenStones', 'findAchievableGrid', 'testAchievable', 'getConquerableGrids');
        }
        GridCollection.prototype.flipInbetweenStones = function (model, val) {
            var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
            var turn = gameModel.get('turn');
            var myColor = turn + 1;

            // 着手した点から8方向を捜査して挟まってたらひっくり返す
            var modelArray = this.getConquerableGrids(model, myColor, true);
            _.each(modelArray, function (model) {
                model.set('status', myColor);
            });

            // ToDo: 本当はターンを司るコントローラを外に作るべき
            // 相手のターン
            turn = turn * -1 + 1;
            gameModel.set('turn', turn);
            return modelArray;
        };

        /***
        有効なマスの発見
        */
        GridCollection.prototype.findAchievableGrid = function () {
            var _this = this;
            var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
            var turn = gameModel.get('turn');
            var myColor = turn + 1;
            this.each(function (model, i) {
                model.set({ 'isAchievable': _this.testAchievable(model, myColor) });
            });
            var model = this.findWhere({ 'isAchievable': true });
            if (!model) {
                alert('パス！');

                // ToDo: 本当はターンを司るコントローラを外に作るべき
                // 相手のターン
                turn = turn * -1 + 1;
                gameModel.set('turn', turn);
                myColor = turn + 1;

                // もっかい探索
                this.each(function (model, i) {
                    model.set({ 'isAchievable': _this.testAchievable(model, myColor) });
                });
                model = this.findWhere({ 'isAchievable': true });
                if (!model) {
                    alert('終了！');
                } else if (turn == 0) {
                    var modelArray = this.where({ 'isAchievable': true });
                    _.each(modelArray, function (model) {
                        model.set({ 'isAchievable': false });
                    });
                    var r = Math.floor(Math.random() * (modelArray.length - 1));
                    setTimeout(function () {
                        _this.flipInbetweenStones(modelArray[r], myColor);
                        modelArray[r].set({ 'status': myColor, 'isAchievable': false });
                        setTimeout(function () {
                            _this.findAchievableGrid();
                        }, 200);
                    }, 1000);
                }
            } else if (turn == 0) {
                var modelArray = this.where({ 'isAchievable': true });
                _.each(modelArray, function (model) {
                    model.set({ 'isAchievable': false });
                });
                var r = Math.floor(Math.random() * (modelArray.length - 1));
                setTimeout(function () {
                    _this.flipInbetweenStones(modelArray[r], myColor);
                    modelArray[r].set({ 'status': myColor, 'isAchievable': false });
                    setTimeout(function () {
                        _this.findAchievableGrid();
                    }, 200);
                }, 1000);
            }
            return model;
        };
        GridCollection.prototype.testAchievable = function (model, myColor) {
            var modelArray = this.getConquerableGrids(model, myColor);
            return (modelArray.length !== 0);
        };
        GridCollection.prototype.getConquerableGrids = function (model, myColor, allFlg) {
            if (typeof allFlg === "undefined") { allFlg = false; }
            var oxy = model.id.split('-');
            var ox = Number(oxy[0]);
            var oy = Number(oxy[1]);
            var x, y;
            var id;
            var i, imax = 1, j, jmax = 1;
            var enemyColor = myColor * -1 + 3;
            var status;
            var stoneArray;
            var modelArray;
            var testModel;
            var returnArray = [];

            // マスが空じゃなかったら探索しない
            if (model.attributes.status !== 0) {
                return returnArray;
            }
            loop1:
            for (i = -1; i <= imax; i++) {
                for (j = -1; j <= jmax; j++) {
                    // 両方0(自分自身)ならスキップ
                    if (i === j && i === 0) {
                        continue;
                    }
                    stoneArray = [];
                    modelArray = [];
                    x = ox;
                    y = oy;
                    do {
                        x += i;
                        y += j;
                        id = x + '-' + y;
                        testModel = this.get(id);
                        if (testModel) {
                            status = testModel.attributes.status;
                            stoneArray.push(status);
                            modelArray.push(testModel);

                            // 敵色の場合のみ続ける。空白と自分の色が入ったら終わる（＝自分の色は最後にしか入らない）
                            if (status !== enemyColor) {
                                break;
                            }
                        } else {
                            break;
                        }
                    } while((x > 0 && x <= 8) && (y > 0 && y <= 8));

                    // 二マス以上且つ自分の色が含まれていたらvalid
                    if (stoneArray.length > 1 && stoneArray.indexOf(myColor) != -1) {
                        Array.prototype.push.apply(returnArray, modelArray);
                        if (!allFlg) {
                            break loop1;
                        }
                    }
                }
            }
            return returnArray;
        };
        return GridCollection;
    })(Backbone.Collection);
    RVS.GridCollection = GridCollection;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
'use strict';
var RVS;
(function (RVS) {
    var GridView = (function (_super) {
        __extends(GridView, _super);
        function GridView(options) {
            if (typeof options === "undefined") { options = {}; }
            _super.call(this, options);
            _.bindAll(this, 'move', 'putStone', 'toggleHighlight');
            this.el.className = 'grid';
            this.el.setAttribute('data-status', '');
            this.listenTo(this.model, 'change:status', this.putStone).listenTo(this.model, 'change:isAchievable', this.toggleHighlight);
            this.events = (typeof this.events == 'object') ? this.events : {};
            this.events = $.extend({
                'click': 'move'
            }, this.events);
            this.delegateEvents();
        }
        GridView.prototype.move = function () {
            var gameModel = RVS.appView.viewModels['boardPageView'].gameModel;
            var turn = gameModel.get('turn');
            var achievable = this.model.get('isAchievable');
            var val;
            if (achievable) {
                val = turn + 1;
                var gridCollection = this.model.collection;
                gridCollection.flipInbetweenStones(this.model, val);
                this.model.set({ 'status': val, 'isAchievable': false });
                gridCollection.findAchievableGrid();
            }
        };
        GridView.prototype.putStone = function (model, val) {
            var status = '';
            if (val === 1) {
                status = 'white';
            } else if (val === 2) {
                status = 'black';
            }
            this.el.setAttribute('data-status', status);
        };
        GridView.prototype.toggleHighlight = function (model, val) {
            this.$el.toggleClass('achievable', val);
        };
        return GridView;
    })(RVS.BaseView);
    RVS.GridView = GridView;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
/// <reference path='../model/GridModel.ts' />
/// <reference path='../collection/GridCollection.ts' />
/// <reference path='./GridView.ts' />
'use strict';
var RVS;
(function (RVS) {
    var BoardPageView = (function (_super) {
        __extends(BoardPageView, _super);
        function BoardPageView(options) {
            var _this = this;
            _super.call(this, options);
            var tds = this.el.querySelectorAll('td');
            var tdArray = Array.apply(null, tds);
            var i, imax = 8, j, jmax = 8;
            var gridId = '';
            var initialData = [];
            var gridData = {};
            for (i = 1; i <= imax; i++) {
                for (j = 1; j <= jmax; j++) {
                    gridId = i + '-' + j;
                    gridData = {};
                    gridData['id'] = gridId;
                    initialData.push(gridData);
                }
            }
            this.gameModel = new Backbone.Model();
            this.listenTo(this.gameModel, 'change:turn', function (model, val) {
                _this.$el.attr('data-turn', val);
            });
            this.gameModel.set({ turn: 1 });
            var gridCollection = new RVS.GridCollection;
            gridCollection.set(initialData);
            gridCollection.each(function (model) {
                var options = {};
                options['model'] = model;
                var view = new RVS.GridView(options);
                var xy = model.attributes.id.split('-');
                var x = Number(xy[0]);
                var y = Number(xy[1]);
                var i = (x - 1) + (y - 1) * 8;
                var td = tdArray[i];
                td.appendChild(view.el);
            });
            gridCollection.get('4-4').set('status', 1);
            gridCollection.get('4-5').set('status', 2);
            gridCollection.get('5-4').set('status', 2);
            gridCollection.get('5-5').set('status', 1);

            // 着手可能枠探索
            setTimeout(function () {
                gridCollection.findAchievableGrid();
            }, 100);
        }
        BoardPageView.prototype._onShow = function () {
        };
        BoardPageView.prototype._onPreShow = function () {
            _super.prototype._onPreShow.call(this);
        };
        return BoardPageView;
    })(RVS.BasePageView);
    RVS.BoardPageView = BoardPageView;
})(RVS || (RVS = {}));
/// <reference path='../main.ts' />
/// <reference path='BaseView.ts' />
/// <reference path='BasePageView.ts' />
/// <reference path='BoardPage.ts' />
'use strict';
var RVS;
(function (RVS) {
    var AppView = (function (_super) {
        __extends(AppView, _super);
        function AppView(options) {
            var _this = this;
            _super.call(this, options);
            this.viewModels = {};
            _.bindAll(this, 'setup');
            setTimeout(function () {
                _this.setup(options);
            }, 0);
        }
        AppView.prototype.setup = function (options) {
            var _this = this;
            var self = this;

            /* 各ページで共通のView,modelを作る */
            /* pageがviewに対応するのでViewをひたすら生成 */
            this.$el.children('[data-role="page"]').each(function (o, el) {
                var id = el.getAttribute('id');
                var options = {};
                options.el = el;
                options.app = _this;
                var varName = RVS.toCamelCase(id + '-view');
                var modelName = RVS.capitaliseFirstLetter(varName);
                if (RVS.hasOwnProperty(modelName)) {
                    self.viewModels[varName] = new RVS[modelName](options);
                } else {
                    self.viewModels[varName] = new RVS.BasePageView(options);
                }
            });
        };
        return AppView;
    })(Backbone.View);
    RVS.AppView = AppView;
})(RVS || (RVS = {}));
/// <reference path='../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/jquerymobile/jquerymobile.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/underscore/underscore.d.ts' />
/// <reference path='../d.ts/DefinitelyTyped/backbone/backbone.d.ts' />
/// <reference path='view/AppView.ts' />

'use strict';
var RVS;
(function (RVS) {
    function capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    RVS.capitaliseFirstLetter = capitaliseFirstLetter;
    function toCamelCase(string) {
        return string.replace(/(\-([a-z]))/g, function ($1, $2, $3) {
            return $3.toUpperCase();
        });
    }
    RVS.toCamelCase = toCamelCase;
    RVS.isTouchSupported = Boolean('ontouchstart' in window && 'orientation' in window);
    RVS.settings = {};
    RVS.appView;
    function initialize() {
        RVS.appView = new RVS.AppView({ el: document.body });
    }
    RVS.initialize = initialize;
    function mobileInit() {
        $.mobile.defaultPageTransition = 'none';
    }
    RVS.mobileInit = mobileInit;
})(RVS || (RVS = {}));
//# sourceMappingURL=main.js.map
