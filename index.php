<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=320,user-scalable=no" id="viewport" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" href="./lib/css/jquery.mobile.structure-1.4.1.min.css" />
<link rel="stylesheet" type="text/css" href="./css/main.css" />
<title>Reversi</title>
</head>
<body>
<?php include_once("board.php"); ?>
<!-- library -->
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="./lib/js/underscore-min.js"></script>
<script src="./lib/js/backbone-min.js"></script>
<script src="./js/main.js"></script>
<script>
$(document).ready(function(){
	RVS.initialize()
}).one('mobileinit',RVS.mobileInit);
</script>
<script src="./lib/js/jquery.mobile-1.4.1.min.js"></script>
</body>
</html>